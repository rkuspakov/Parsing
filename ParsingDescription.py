from bs4 import BeautifulSoup
import pandas as pd
import os



def parsing_description():
    alldata = []
    for row in range(1, 41):
        filename = str(row) + '.html'
        if os.path.exists("Description\\" + filename) == True:
            print("---" + str(filename))
            with open("Description\\" + filename, mode="r", encoding="utf-8") as f:
                soup = BeautifulSoup(f, 'html.parser')

            tables = soup.findAll('table', attrs={'class': 'scroll_nr'})

            for table in tables:
                dt = []
                table_body = table.find('tbody')
                rows = table_body.find_all('tr')
                for row in rows:
                    cols = row.find_all('td')
                    if str(cols[0].contents[0].contents[0]).find('img') > 0:
                        dt.append(cols[0].contents[0].attrs['href'])
                        dt.append(cols[0].contents[0].contents[0].attrs['src'])
                        dt.append(cols[0].contents[0].contents[0].attrs['alt'])
                        alldata.append(dt)

    df = pd.DataFrame(alldata)
    df.to_excel(r"output.xlsx")


if __name__ == "__main__":
    parsing_description()
