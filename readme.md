## Структура папок

PARSING
│   Category.py
│   Description.py
│   output.xlsx
│   ParsingCategory.py
│   ParsingDescription.py
│   readme.md
│   tree.txt
│   
├───Category
│       Category.html
│       
├───Description
│       Description.html
│       
└───EdgeDriver
        msedgedriver.exe

## Принцип отработки скриптов следующий:

Производиться скачивание структуры каталога (Category.py) в папку Category
Производится парсинг структуры каталога (ParsingCategory.py)
Производиться скачивание всех товаров (Description.py) в папку Description
Производится парсинг описания (ParsingDescription.py)
