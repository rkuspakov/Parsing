from bs4 import BeautifulSoup
import pandas as pd
import os


def parsing_category():
    alldata = []
    directory = os.fsencode("Category\\")
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if os.path.exists("Category\\" + filename) == True:
            print("---" + str(filename))
            with open("Category\\" + filename, mode="r", encoding="utf-8") as f:
                soup = BeautifulSoup(f, 'html.parser')

            tables = soup.findAll('table', attrs={'class': 'scroll_nr'})

            for table in tables:
                dt = []
                table_body = table.find('tbody')
                rows = table_body.find_all('tr')
                for row in rows:
                    cols = row.find_all('td')
                    if str(cols[0].contents[0].contents[0]).find('img') > 0:
                        dt.append(cols[0].contents[0].attrs['href'])
                        dt.append(cols[0].contents[0].contents[0].attrs['src'])
                        dt.append(cols[0].contents[0].contents[0].attrs['alt'])
                        dt.append(filename)
                        alldata.append(dt)

    df = pd.DataFrame(alldata)
    df.to_excel(r"output.xlsx")
    

if __name__ == "__main__":
    parsing_category()