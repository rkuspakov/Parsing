# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from threading import Thread
import os.path
import time
import sqlite3 as sl
import pandas as pd


class ThreadDownload(Thread):
    """
    A threading example
    """


    def __init__(self, page):
        """Инициализация потока"""
        Thread.__init__(self)
        self.page = page
        self.type = type

    def run(self):
        ls = []
        filename = str(self.page) + ".html"
        ls.append(filename)
        if not os.path.exists("Description\\" + filename):
            url = 'http://www.nbschoolab.com/en/products.php?page=' + str(self.page)
            print(url)
            service = Service(executable_path='Edgedriver\msedgedriver.exe')
            browser = webdriver.Edge(service=service)
            try:
                browser.get(url)
                time.sleep(10)
                with open("Description\\" + filename, "w",
                          encoding='utf-8') as f:
                    f.write(browser.page_source)
                browser.quit()
            except Exception as ex:
                print(ex)
                browser.quit()
        else:
            print(filename)

        df = pd.DataFrame(ls)
        df.to_excel("output.xlsx")

def create_threads():
    """
    Запускаем программу
    """
    for page in range(1, 41):
        # print(page * 10)
        thread = ThreadDownload(page)
        thread.start()


if __name__ == "__main__":
    create_threads()
